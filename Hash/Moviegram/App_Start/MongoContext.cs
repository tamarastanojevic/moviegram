﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Moviegram.App_Start
{
    public class MongoContext
    {
        MongoClient _client;
        MongoServer _server;
        public MongoDatabase _database;

        public MongoContext()
        {
            var mongoDBName = ConfigurationManager.AppSettings["MongoDatabaseName"];
            var mongoPort = ConfigurationManager.AppSettings["MongoPort"];
            var mongoHost = ConfigurationManager.AppSettings["MongoHost"];

            var settings = new MongoClientSettings
            {
                Server = new MongoServerAddress(mongoHost, Convert.ToInt32(mongoPort))
            };

            _client = new MongoClient(settings);
            _server = _client.GetServer();
            _database = _server.GetDatabase(mongoDBName);
        }
    }
}