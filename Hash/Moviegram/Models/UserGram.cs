﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Moviegram.Models
{
    public class UserGram
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Username")]
        public string Username { get; set; }
        [BsonElement("Password")] 
        public string Password { get; set; }
        [BsonElement("FullName")]
        public string FullName { get; set; }
        [BsonElement("Bio")]
        public string Bio { get; set; }
        [BsonElement("Posts")]
        public List<PostGram> Posts { get; set; }
        [BsonElement("FollowedTags")]
        public List<Tuple<ObjectId, string>> FollowedTags { get; set; }

        public UserGram()
        {
            Posts = new List<PostGram>();
            FollowedTags = new List<Tuple<ObjectId, string>>();
        }
    }
}