﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Moviegram.Models
{
    public class CommentsGram
    {
        [BsonElement("Id")]
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("CommId")]
        public Guid CommId { get; set; }

        [BsonElement("OwnerId")]
        public ObjectId OwnerId { get; set; }
        [BsonElement("OwnerUsername")]
        public string OwnerUsername { get; set; }

        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }
        [BsonElement("Text")]
        public string Text { get; set; }

        public CommentsGram()
        {
            CommId = Guid.NewGuid();
        }
    }
}