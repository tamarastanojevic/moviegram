﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Moviegram.Models
{
    public class HashtagGram
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("Timestamp")]
        public BsonDateTime Timestamp { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("Posts")]
        public List<PostGram> Posts { get; set; }

        public HashtagGram()
        {
            Posts = new List<PostGram>();
        }

    }
}