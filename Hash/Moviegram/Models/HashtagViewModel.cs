﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Moviegram.Models
{
    public class HashtagViewModel
    {
        public UserGram user;
        public HashtagGram hashtag;
        public bool following;

        public HashtagViewModel()
        {
            user = new UserGram();
            hashtag = new HashtagGram();
            following = false;
        }
    }
}